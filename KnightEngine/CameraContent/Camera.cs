﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameEngine.GameObjects;
using GameEngine.MapContent;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace GameEngine.CameraContent
{
     public class Camera
    {

        #region Konstruktor

        public Camera(Viewport viewport, Entity entity)
        {
            this.entity = entity;
            this.cameraPosition = new Vector2(viewport.Width/2, viewport.Height/2);
            Bounds = viewport.Bounds;
            Zoom = 1f;

            UpdateMatrix();
        }

        #endregion

        #region Props

        private Entity entity;

        private float currentMouseWheelValue, previousMouseWheelValue, zoom, previousZoom;

        private Vector2 cameraPosition;

        #endregion

        #region GetterSetter

        public float Zoom { get; private set; }
        public Rectangle Bounds { get; private set; }
        public Rectangle VisibleArea { get; private set; }

        internal void SetTarget(Entity entity)
        {
            this.entity = entity;
        }

        public Matrix Transform { get; private set; }

        #endregion

        #region Methods

        public Point MousclickTransformToViewMatrix(Point mousePosition)
        {
            Vector2 worldPosition = Vector2.Transform(new Vector2(mousePosition.X, mousePosition.Y), Matrix.Invert(Transform));
            return new Point((int)worldPosition.X, (int)worldPosition.Y);
        }
       
        private void UpdateVisibleArea()
        {
            var inverseViewMatrix = Matrix.Invert(Transform);

            var tl = Vector2.Transform(Vector2.Zero, inverseViewMatrix);
            var tr = Vector2.Transform(new Vector2(Bounds.X, 0), inverseViewMatrix);
            var bl = Vector2.Transform(new Vector2(0, Bounds.Y), inverseViewMatrix);
            var br = Vector2.Transform(new Vector2(Bounds.Width, Bounds.Height), inverseViewMatrix);

            var min = new Vector2(
                MathHelper.Min(tl.X, MathHelper.Min(tr.X, MathHelper.Min(bl.X, br.X))),
                MathHelper.Min(tl.Y, MathHelper.Min(tr.Y, MathHelper.Min(bl.Y, br.Y))));
            var max = new Vector2(
                MathHelper.Max(tl.X, MathHelper.Max(tr.X, MathHelper.Max(bl.X, br.X))),
                MathHelper.Max(tl.Y, MathHelper.Max(tr.Y, MathHelper.Max(bl.Y, br.Y))));
            VisibleArea = new Rectangle((int)min.X, (int)min.Y, (int)(max.X - min.X), (int)(max.Y - min.Y));
        }

        private void UpdateMatrix()
        {
            
            Transform = Matrix.CreateTranslation(new Vector3(-this.cameraPosition.X, -this.cameraPosition.Y, 0)) *
                    Matrix.CreateScale(Zoom) *
                    Matrix.CreateTranslation(new Vector3(Bounds.Width * 0.5f, Bounds.Height * 0.5f, 0));
            UpdateVisibleArea();
        }

        public void MoveCamera(Vector2 movePosition)
        {
            UpdateMatrix();
        }

        public void AdjustZoom(float zoomAmount)
        {
            Zoom += zoomAmount;
            if (Zoom < 0.8f)
            {
                Zoom = 0.8f;
            }
            if (Zoom > 3f)
            {
                Zoom = 3f;
            }
        }

        public void UpdateCamera()
        {
            if (entity == null) return;
           

            previousMouseWheelValue = currentMouseWheelValue;
            currentMouseWheelValue = Mouse.GetState().ScrollWheelValue;

            if (currentMouseWheelValue > previousMouseWheelValue)
            {
                AdjustZoom(0.2f);
            }

            if (currentMouseWheelValue < previousMouseWheelValue)
            {
                AdjustZoom(-0.2f);
            }

            previousZoom = zoom;
            zoom = Zoom;
          
            if (previousZoom != zoom || entity.GameObjectPos != this.cameraPosition)
            {
                this.cameraPosition = entity.GameObjectPos;
                UpdateMatrix();

            }


        }

        #endregion

    }
}
