﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace GameEngine.GuiContent
{
    public class Button : Component
    {
        #region Konstruktor

        public Button(SpriteFont font, Point pos, String text)
        {

            this.text = text;
            _font = font;

            button = new Rectangle(pos.X, pos.Y, 100, 50);
        }

        #endregion

        #region Fields

        private MouseState _currentMouse;

        private SpriteFont _font;

        private bool _isHovering;

        private MouseState _previousMouse;


        private Rectangle button;

        #endregion

        #region Properties

        public EventHandler Click;
        public bool Clicked { get; private set; }

        private string text;

        #endregion

        #region Methods

       




        public override void Draw( SpriteBatch spriteBatch)
        {
            var color = Color.White;

            if (_isHovering)
                color = Color.Gray;

            spriteBatch.Draw(KnightEngine.rect, button, color);

            if (!string.IsNullOrEmpty(text))
            {
                var x = (button.X + (button.Width / 2)) - (_font.MeasureString(text).X / 2);
                var y = (button.Y + (button.Height / 2)) - (_font.MeasureString(text).Y / 2);

                spriteBatch.DrawString(_font, text, new Vector2(x, y), Color.Black);
            }
        }

        public override void Update()
        {
            _previousMouse = _currentMouse;
            _currentMouse = Mouse.GetState();

            var mouseRectangle = new Rectangle(_currentMouse.X, _currentMouse.Y, 1, 1);

            _isHovering = false;

            if (mouseRectangle.Intersects(button))
            {
                _isHovering = true;

                if (_currentMouse.LeftButton == ButtonState.Released && _previousMouse.LeftButton == ButtonState.Pressed)
                {
                    Click?.Invoke(this, new EventArgs());
                }
            }
        }

        #endregion
    }
}
