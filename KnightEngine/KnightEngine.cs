﻿using GameEngine.CameraContent;
using GameEngine.GameObjects;
using GameEngine.MapContent;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameEngine
{
    public class KnightEngine : GameComponent
    {


        public KnightEngine(Game game) : base(game)
        {
            Load(game.GraphicsDevice, game.Content);
            
            entities = new List<Entity>();
        }

    

        private List<Entity> entities;
        public static Texture2D rect;
        private Map map;
        private ContentManager content;
        private Camera camera;
        private void Load(GraphicsDevice graphicsDevice,ContentManager content)
        {
            rect = new Texture2D(graphicsDevice, 1, 1);
            rect.SetData(new[] { Color.White });
            this.content = content;
            camera = new Camera(graphicsDevice.Viewport,null);

        }

        public void LoadMap(String map)
        {
            this.map = new Map(map, content);
        }

        public void SetCameraTarget(Entity entity)
        {
            camera.SetTarget(entity);
        }

        internal Map GetActualMap()
        {
           
            return map;
        }

        public override void Update(GameTime gameTime)
        {
            map.Update(gameTime.ElapsedGameTime.Seconds, camera);

            foreach (var entity in entities)
            {
                entity.Update(gameTime.ElapsedGameTime.Milliseconds,map);
            }

            camera.UpdateCamera();
        }

        public void Draw(GameTime gameTime,SpriteBatch spriteBatch)
        {
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp, null, null, null, camera.Transform);

            map.DrawMap(spriteBatch, true);

            foreach (var entity in entities)
            {
                entity.Draw(spriteBatch);
            }

            map.DrawMap(spriteBatch, false);

            spriteBatch.End();
        }

        public void AddEntity(Entity entity)
        {
            entities.Add(entity);
        }
    }
}
