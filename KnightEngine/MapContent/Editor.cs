﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using GameEngine.CameraContent;
using GameEngine.GuiContent;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace GameEngine.MapContent
{
    /// <summary>
    /// Editor kann die aktuelle Karte verändern, dieser wird mit f2 im Spiel aufgerufen, mit f1 kann dan abgespeichert werden. Mit f3,f4 kann der layer gewechselt werden
    /// </summary>
    public class Editor
    {
        #region Konstruktor

        public Editor(KnightEngine engine,ContentManager content)
        {

            this.engine = engine;
            actualTileSet = engine.GetActualMap().TextureSheet;
            actualTileSize = engine.GetActualMap().Tilesize;
            editorWindow = new Rectangle(new Point(screenwidth - actualTileSet.Width - 20, 0), new Point(actualTileSet.Width + 20, actualTileSet.Height + 20));
            editorTileset = new Rectangle(new Point(screenwidth - actualTileSet.Width, 0), new Point(actualTileSet.Width, actualTileSet.Height));
            selectedTile = new Rectangle(new Point(screenwidth - actualTileSet.Width, 0), new Point(actualTileSize, actualTileSize));
            font = content.Load<SpriteFont>("Arial");
       
        }











        #endregion

        #region Porps

        KnightEngine engine;
        private SpriteFont font;
      
        private Texture2D actualTileSet;
        private Rectangle selectedTile;
        
        private Rectangle editorWindow;
        private Rectangle editorTileset;
        private int actualTileSize;
        private ushort actualSelectedTile;
        private MouseState mouseState;
        private KeyboardState kbState;
        private int edetingLayer = 0;
        private int screenwidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
        private int mode = 0;
        private String[] modes = {"TileMode","CollisionMode","ObjectMode" };
        private List<Component> components = new List<Component>();

        //debug zwecke


        #endregion

        #region GetterSetter

        #endregion

        #region Methods

        private void ButtonPressed(object sender, EventArgs e)
        {
            
        }
        public void Update(Camera camera)
        {
           

            foreach (var component in components)
            {
                component.Update();
            }

            EditorFunctions(camera);
        }

        private void EditorFunctions(Camera camera)
        {
            KeyboardState lastkbState = kbState;
            kbState = Keyboard.GetState();



            if (kbState.IsKeyDown(Keys.F1) && lastkbState.IsKeyUp(Keys.F1))
            {
                SafeMap();
            }

            if (kbState.IsKeyDown(Keys.F5) && lastkbState.IsKeyUp(Keys.F5))
            {
                mode++;
                if (mode > 2) mode = 0;
                engine.GetActualMap().DrawCollisionLayer = false;

                if (mode==0) {
                    actualTileSet = engine.GetActualMap().TextureSheet;
                }
                else if(mode==1)
                {
                    actualTileSet = engine.GetActualMap().CollisionLayer.CollisionTilesSprite;
                    engine.GetActualMap().DrawCollisionLayer = true;
                }
                else if (mode == 2)
                {
                    actualTileSet = engine.GetActualMap().TextureSheet;
                }

                actualSelectedTile = 0;
                editorWindow = new Rectangle(new Point(screenwidth - actualTileSet.Width - 20, 0), new Point(actualTileSet.Width + 20, actualTileSet.Height + 20));
                editorTileset = new Rectangle(new Point(screenwidth - actualTileSet.Width, 0), new Point(actualTileSet.Width, actualTileSet.Height));
                selectedTile = new Rectangle(new Point(screenwidth - actualTileSet.Width, 0), new Point(actualTileSize, actualTileSize));
            }


            ScrollMap();
          
            ChangeMapTile(camera);
             
            
            
            ChangeMapLayer(lastkbState);
            
        }

        private void ScrollMap()
        {

        }

        private void ChangeMapLayer(KeyboardState lastkbState)
        {
            if (kbState.IsKeyDown(Keys.F3) && lastkbState.IsKeyUp(Keys.F3))
            {
                if (edetingLayer > 0)
                    edetingLayer--;
            }
            if (kbState.IsKeyDown(Keys.F4) && lastkbState.IsKeyUp(Keys.F4))
            {
                if (edetingLayer < engine.GetActualMap().Maplayer.Length - 1)
                    edetingLayer++;
            }
        }

        private void ChangeMapTile(Camera camera)
        {
            MouseState mouseStatebefore = mouseState;
            mouseState = Mouse.GetState();
            SelectTileFromTileSheet(mouseStatebefore);
            if (mouseState.LeftButton == ButtonState.Pressed)
            {

                SetTileToMap(camera,mouseState, false);

            }
            else if (mouseState.RightButton == ButtonState.Pressed)
            {

                SetTileToMap(camera, mouseState, true);

            }
        }

        private void SetTileToMap(Camera camera,MouseState mouseState, bool loeschen)
        {

            var pos = mouseState.Position;
            if (pos.X < screenwidth - actualTileSet.Width || pos.Y > actualTileSet.Height)
            {

                var layer = engine.GetActualMap().Maplayer;
                var translatedMousePos = camera.MousclickTransformToViewMatrix(pos);
                if(!engine.GetActualMap().DrawCollisionLayer)
                layer[edetingLayer].SetIDForTile(translatedMousePos.X  / actualTileSize, translatedMousePos.Y  / actualTileSize, actualSelectedTile, loeschen, engine.GetActualMap().Mapsize);
                else
                    engine.GetActualMap().CollisionLayer.SetIDForTile(translatedMousePos.X / actualTileSize, translatedMousePos.Y / actualTileSize, actualSelectedTile, loeschen, engine.GetActualMap().Mapsize);
            }




        }

        private void SafeMap()
        {

            XMLData.MapData testData = new XMLData.MapData();
            testData.MapHeight = engine.GetActualMap().Mapsize.Y;
            testData.MapWidth = engine.GetActualMap().Mapsize.X;
            testData.TilesetName = engine.GetActualMap().TileSetName;
            //Save MapLayer

            #region SaveMapLayer

            MapLayer[] maplayer = engine.GetActualMap().Maplayer;

            int width = engine.GetActualMap().Mapsize.X;
            int height = engine.GetActualMap().Mapsize.Y;


            testData.MapLayerData = new XMLData.MapLayerData[maplayer.Length];
            for (int i = 0; i < maplayer.Length; i++)
            {
                testData.MapLayerData[i] = new XMLData.MapLayerData();
                testData.MapLayerData[i].LayerID = maplayer[i].LayerID;
                testData.MapLayerData[i].BeforePlayer = maplayer[i].BeforePlayer;
            
                var layerTiles = maplayer[i].LayerTiles;
                String dummy = CreateStringFromMap(height,width, layerTiles);
                testData.MapLayerData[i].MapTileArray = dummy;
            }

            #endregion


            testData.CollisionLayer = CreateStringFromMap(width,height, engine.GetActualMap().CollisionLayer.CollisionTiles);


            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;

            using (XmlWriter writer = XmlWriter.Create("../../../../Content/" + engine.GetActualMap().Mapname + ".xml", settings))
            {
                writer.WriteString(testData.ToString());
               
            }
        }

        private string CreateStringFromMap(int width,int height,int[] layerTiles)
        {
            string dummy = "\n";
            for (int y = 0; y < height - 1; y++)
            {
                for (int x = 0; x < width - 1; x++)
                {

                    if (layerTiles[y * width + x] == -1)
                        dummy += "-,";
                    else
                        dummy += layerTiles[y * width + x] + ",";
                }
                if (layerTiles[y * width + width - 1] == -1)
                    dummy += "-;\n";
                else
                    dummy += layerTiles[y * width + width - 1] + ";\n";

            }
            return dummy;
        }

        private void SelectTileFromTileSheet(MouseState mouseStatebefore)
        {

            if (mouseStatebefore.LeftButton == ButtonState.Released && mouseState.LeftButton == ButtonState.Pressed)
            {
                var pos = mouseState.Position;
                if (pos.X > screenwidth - actualTileSet.Width && pos.Y < actualTileSet.Height)
                {
                    int xPos = (pos.X - screenwidth + actualTileSet.Width) / actualTileSize;
                    int yPos = pos.Y / actualTileSize;
                    actualSelectedTile = (ushort)(yPos * (actualTileSet.Width / actualTileSize) + xPos);
                    selectedTile = new Rectangle(new Point(screenwidth - actualTileSet.Width + xPos * actualTileSize, yPos * actualTileSize), new Point(actualTileSize, actualTileSize));
                }
            }
        }

        internal void Draw(SpriteBatch spriteBatch)
        {

            spriteBatch.Draw(KnightEngine.rect, editorWindow, Color.White);
            spriteBatch.Draw(actualTileSet, editorTileset, Color.White);
            spriteBatch.Draw(KnightEngine.rect, selectedTile, Color.Red * 0.6f);
            spriteBatch.DrawString(font, "aktueller Layer:"+ (edetingLayer+1) +"         InFrontOfPlayer:" +engine.GetActualMap().Maplayer[edetingLayer].BeforePlayer+"     Mode:"+ modes[mode], new Vector2(20, 20), Color.White);
            foreach (var component in components)
            {
                component.Draw(spriteBatch);
            }

        }

        #endregion

    }
}
