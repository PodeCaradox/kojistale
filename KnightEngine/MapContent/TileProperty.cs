﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace GameEngine.MapContent
{
    /// <summary>
    /// Hier werden die Werte für eine Tile ID gespeichert. Es kann angegeben werden ob diese den Player beschleunigen oder verlangsamen usw.
    /// </summary>
    class TileProperty
    {

        #region Konstruktor


        public TileProperty(int iD, int property, Rectangle textureSheetPosition)
        {
            this.id = iD;
            this.property = (Property)property;
            this.textureSheetPosition = textureSheetPosition;
        }

        #endregion

        #region Props

        public enum Property {Static=0,Stone=1,Snow=2 }

        private Rectangle textureSheetPosition;

        private int id;

        private Property property;




        #endregion

        #region GetterSetter

        public Rectangle TextureSheetPosition { get => textureSheetPosition; set => textureSheetPosition = value; }
        public int Id { get => id; set => id = value; }
        internal Property Property1 { get => property; set => property = value; }


        #endregion

    }
}
