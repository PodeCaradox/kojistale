﻿using GameEngine.SharpMath2;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using XMLData;

namespace GameEngine.MapContent
{
    public class CollisionLayer
    {
        private Texture2D collisionTilesSprite;
        private Shape2[] collisionShape;
        private int[] collisionTiles;
        private int tilesize;
        private int width;

        public CollisionLayer(String map, int tilesize, Point mapsize, ContentManager content)
        {
            collisionTilesSprite = content.Load<Texture2D>("CollisionTilesTex");
            width = mapsize.X;
            this.tilesize = tilesize;
            collisionTiles = ConvertStringMapToArray(map, tilesize, mapsize);


            XMLData.CollisionTilesData[] collisionShapes = content.Load<XMLData.CollisionTilesData[]>("CollisionTiles");

            InitShapes(collisionShapes);
           
        }

        private void InitShapes(CollisionTilesData[] collisionShapes)
        {
            collisionShape = new Shape2[collisionShapes.Length];

            for (int i = 0; i < collisionShapes.Length; i++)
            {
                if (collisionShapes[i].Shape.ToUpper() == "Rectangle".ToUpper())
                {
                    collisionShape[i] = new Rect2(new Vector2(collisionShapes[i].Points[0], collisionShapes[i].Points[1]), new Vector2(collisionShapes[i].Points[2], collisionShapes[i].Points[3]));
                }
                else
                {
                    List<Vector2> vectors = new List<Vector2>();
                    for (int j = 0; j < collisionShapes[i].Points.Length; j += 2)
                    {
                        vectors.Add(new Vector2(collisionShapes[i].Points[j], collisionShapes[i].Points[j + 1]));
                    }


                    collisionShape[i] = new Polygon2(vectors.ToArray());
                }
            }

        }

        public Texture2D CollisionTilesSprite { get => collisionTilesSprite;  }

        internal int[] CollisionTiles { get => collisionTiles; }

        private static int[] ConvertStringMapToArray(String map, int tilesize, Point mapsize)
        {

            int[] mapTiles = new int[mapsize.Y * mapsize.X];
            map = map.Replace("\t", String.Empty).Replace("\n", String.Empty);
            String[] lines = map.Split(';');

            //delete last one because its empty 
            lines = lines.Where(val => val != String.Empty).ToArray();
            int x = 0;
            int y = 0;
            foreach (var line in lines)
            {

                string[] numbers = line.Split(',');
                foreach (string e in numbers)
                {

                    int tile = 0;
               

                    if (!int.TryParse(e, out tile))
                    {
                        Console.WriteLine("!----- Error to create tile ID from CollisionLayer.");
                        mapTiles[y * mapsize.X + x] = -1;
                    }
                    else
                    {
                        mapTiles[y * mapsize.X + x] = tile;
                    }


                       

                 




                    x++;
                }
                x = 0;
                y++;
            }


            return mapTiles;
        }

        private Vector2 CalculatePosition(int pos)
        {
           return new Vector2((int)(pos  % width * tilesize), (int)(pos  / width * tilesize));
        }

        public void SetIDForTile(int ArrayxPos, int ArrayyPos, ushort id, bool loeschen, Point mapsize)
        {
            if (ArrayyPos < mapsize.Y && ArrayxPos < mapsize.X && ArrayxPos >= 0 && ArrayyPos >= 0)
            {
                if (loeschen)
                {
                    collisionTiles[ArrayyPos * mapsize.X + ArrayxPos] = -1;
                    return;
                }


                
                    collisionTiles[ArrayyPos * mapsize.X + ArrayxPos] = id;
         

                Console.WriteLine("Tile  with id " + id + " set to CollisionLayer");
            }
            else
            {
                Console.WriteLine("Tile konnte nicht gesetzt werden da Map zu klein ist bitte Map größe ändern");
            }
        }

        public bool IsCollisionWithWorld(Vector2 pos,ref Vector2 velocitiy,Rect2 collisionRectangle,bool? left,bool? up)
        {
            bool collision = false;
            try
            {

    
                if (up == null)
                {
                    float x;
                    float y1= pos.Y + collisionRectangle.Min.Y;
                    float y2= pos.Y + collisionRectangle.Max.Y;
                    if (left??true)
                    {
                        x = pos.X - velocitiy.X + collisionRectangle.Min.X;
                       
                        pos.X -= velocitiy.X;
                     
                    }
                    else
                    {
                        x = pos.X + velocitiy.X + collisionRectangle.Max.X;

                        pos.X += velocitiy.X;
                     
                    }
                    var dummy = collisionTiles[(int)x / tilesize % width + (int)y1 / tilesize * width];
                    var dummy1 = collisionTiles[(int)x / tilesize % width + (int)y2 / tilesize * width];
                    if (dummy != -1)
                    {
                        if(collisionShape[dummy] is Rect2)
                        {
                            if (dummy == 0) collision = true;
                            else collision = true;
                        }
                        else
                        {
                            var move=Polygon2.IntersectMTV(collisionRectangle, collisionShape[dummy] as Polygon2, pos, CalculatePosition(dummy));//todo 
                            if (move != null)
                            {
                                velocitiy = Vector2.Zero;
                            }
                        }
                    
                    }else if (dummy1 != -1)
                    {
                        if (collisionShape[dummy1] is Rect2)
                        {
                            if (dummy1 == 0) collision = true;
                            else collision = true;
                        }
                        else
                        {
                            var move = Polygon2.IntersectMTV(collisionRectangle, collisionShape[dummy1] as Polygon2, pos, CalculatePosition(dummy1));
                            if (move != null) velocitiy = Vector2.Zero;
                        }
                    }


                }

                if (left == null)
                {
                    float y;
                    float x1 = pos.X + collisionRectangle.Min.X;
                    float x2 = pos.X + collisionRectangle.Max.X;
                    if (up ?? true)
                    {
                        y = pos.Y - velocitiy.Y + collisionRectangle.Min.Y;
                        pos.X += velocitiy.Y;
                    }
                    else
                    {
                        y = pos.Y + velocitiy.Y + collisionRectangle.Max.Y;
                        pos.X += velocitiy.Y;
                    }
                    var dummy = collisionTiles[(int)x1 / tilesize % width + (int)y / tilesize * width];
                    var dummy1 = collisionTiles[(int)x2 / tilesize % width + (int)y / tilesize * width];
                    if (dummy != -1)
                    {
                        if (collisionShape[dummy] is Rect2)
                        {
                            if(dummy==0) collision = true;
                            else collision = true;
                        }
                        else
                        {
                            var move = Polygon2.IntersectMTV(collisionRectangle, collisionShape[dummy] as Polygon2, pos, CalculatePosition(dummy));
                            if (move != null) velocitiy = Vector2.Zero;
                        }

                    }
                    else if (dummy1 != -1)
                    {
                        if (collisionShape[dummy1] is Rect2)
                        {
                            if (dummy1 == 0) collision = true;
                            else collision = true;
                        }
                        else
                        {
                            var move = Polygon2.IntersectMTV(collisionRectangle, collisionShape[dummy1] as Polygon2, pos, CalculatePosition(dummy1));
                            if (move != null) velocitiy = Vector2.Zero;
                        }
                    }
            
                }

            }
            catch (Exception)
            {

                collision = true;
            }

            return collision;

        }

    }
}
