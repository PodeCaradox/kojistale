﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameEngine.CameraContent;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace GameEngine.MapContent
{
    /// <summary>
    /// Innerhalb einer Map werden Layer gespeichert die ein Array aus Maptiles haben, hier wird auch berechnet wie lange die Draw Methode höschstens Laufen muss
    /// </summary>
    public class Map
    {
        #region Konstruktor

        public Map(String mapname, ContentManager content)
        {
            this.mapname = mapname + "-map";
            XMLData.MapData map = content.Load<XMLData.MapData>(this.mapname);
            XMLData.TileSetData tileset = content.Load<XMLData.TileSetData>(map.TilesetName);
            tileSetName = map.TilesetName;

        
            textureSheet = content.Load<Texture2D>(tileset.TextureName+"-summer");

            this.tilesize = tileset.TileSize;
            tileProperty = new TileProperty[tileset.TileProperty.Length];
            InitTilePropertys(tileset, textureSheet.Width);
            mapsize = new Point(map.MapWidth, map.MapHeight);


            maplayer = new MapLayer[map.MapLayerData.Length];
            for (int i = 0; i < maplayer.Length; i++)
            {
                maplayer[i] = new MapLayer(map.MapLayerData[i], this.mapname, tilesize, mapsize);
            }

            collisionLayer = new CollisionLayer(map.CollisionLayer,tilesize, mapsize,content);

        }

        #endregion

        #region Props
        #if DEBUG
        private bool drawCollisionLayer = false;

        #endif

      
        private CollisionLayer collisionLayer;
        private MapLayer[] maplayer;
        private Texture2D textureSheet;
        private String mapname;
        private int tilesize;
        private TileProperty[] tileProperty;
        private Point YMapZeichnen, XMapZeichnen;
        private Point mapsize;
        private String tileSetName;
        #endregion

        #region GetterSetter

        internal Texture2D TextureSheet { get => textureSheet; }
        internal MapLayer[] Maplayer { get => maplayer; }
        internal int Tilesize { get => tilesize; }
        internal string Mapname { get => mapname; }
        internal Point Mapsize { get => mapsize; }
        internal TileProperty[] TileProperty { get => tileProperty; }
        public string TileSetName { get => tileSetName; }
        public CollisionLayer CollisionLayer { get => collisionLayer; }

        #if DEBUG
        public bool DrawCollisionLayer { get => drawCollisionLayer; set => drawCollisionLayer = value; }
        #endif

        #endregion

        #region Methods

        private void InitTilePropertys(XMLData.TileSetData tileSet, int tilesetWidth)
        {
            
            tilesetWidth /= tileSet.TileSize;
            for (int i = 0; i < tileSet.TileProperty.Length; i++)
            {

                int x = (i % tilesetWidth) * tileSet.TileSize;
                int y = (i / tilesetWidth) * tileSet.TileSize;

                this.tileProperty[i] = new TileProperty(tileSet.TileProperty[i].ID, tileSet.TileProperty[i].Property, new Rectangle(new Point(x, y), new Point(tileSet.TileSize, tileSet.TileSize)));
            }

            //safeTile = tileProperty[355].TextureSheetPosition;

        }

        public void Update(int elapsedTime, Camera camera)
        {
            
            CalculateLayerLoop(camera);
        }
       
        /// <summary>
        /// Calculate the drawing Loop Tiles that visible, calculate start and endpoint
        /// </summary>
        /// <param name="camera"></param>
        private void CalculateLayerLoop(Camera camera)
        {

            XMapZeichnen = new Point((int)camera.VisibleArea.X / tilesize - 1, (int)(camera.VisibleArea.X + camera.VisibleArea.Width) / tilesize + 2);
            if (XMapZeichnen.X < 0) XMapZeichnen.X = 0;
            if (XMapZeichnen.Y > mapsize.X) XMapZeichnen.Y = mapsize.X;

            YMapZeichnen = new Point((int)camera.VisibleArea.Y / tilesize - 1, (int)(camera.VisibleArea.Y + camera.VisibleArea.Height) / tilesize + 2);
            if (YMapZeichnen.X < 0) YMapZeichnen.X = 0;
            if (YMapZeichnen.Y > mapsize.Y) YMapZeichnen.Y = mapsize.Y;

        }

        private Vector2 CalculatePosition(int pos)
        {
            return new Vector2((int)(pos % mapsize.X * tilesize), (int)(pos / mapsize.X * tilesize));
        }


        /// <summary>
        /// All Tiles from the Layer will be drawn here
        /// </summary>
        /// <param name="spriteBatch"></param>
        /// <param name="beforePlayer">terminate if the layer is drawn before or after the player</param>
        public void DrawMap(SpriteBatch spriteBatch,bool beforePlayer)
        {

            foreach (var layer in maplayer)
            {
                if (layer.BeforePlayer != beforePlayer) continue;
                for (int i = YMapZeichnen.X; i < YMapZeichnen.Y; i++)
                {
                    var tile = i * mapsize.X;
                    for (int j = XMapZeichnen.X; j < XMapZeichnen.Y; j++)
                    {
                        if (layer.LayerTiles[tile + j] == -1) continue;
                        spriteBatch.Draw(textureSheet, CalculatePosition(tile + j), tileProperty[layer.LayerTiles[tile + j]].TextureSheetPosition, Color.White);

                    }
                }

            }

            #if DEBUG
            if (drawCollisionLayer)
            {
                var point = new Point(tilesize, tilesize);

                for (int i = YMapZeichnen.X; i < YMapZeichnen.Y; i++)
                {
                    var tile = i * mapsize.X;
                    for (int j = XMapZeichnen.X; j < XMapZeichnen.Y; j++)
                    {
                        if (collisionLayer.CollisionTiles[tile + j] == -1) continue;
                        spriteBatch.Draw(collisionLayer.CollisionTilesSprite, CalculatePosition(tile + j), new Rectangle(new Point((collisionLayer.CollisionTiles[tile + j] * tilesize) % collisionLayer.CollisionTilesSprite.Width, (collisionLayer.CollisionTiles[tile + j] * tilesize) / collisionLayer.CollisionTilesSprite.Width), point), Color.White);

                    }
                }

            }
            #endif
        }

        #endregion

    }
}
