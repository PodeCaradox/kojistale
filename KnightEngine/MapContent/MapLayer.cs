﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameEngine.MapContent
{
    /// <summary>
    /// There are diffrent Layers some before the player and some behind of it
    /// </summary>
    class MapLayer 
    {

        #region Konstruktor

        public MapLayer(XMLData.MapLayerData mapData, string mapname, int tilesize, Point mapsize)
        {
            this.beforePlayer = mapData.BeforePlayer;
            this.layerID = (byte)mapData.LayerID;
           
            this.tilesize = tilesize;
            layerTiles = ConvertStringMapToArray(mapData, mapname, tilesize, mapsize, mapData.LayerID);
          

        }

        /// <summary>
        /// Convert the map from String in a MapTile Array
        /// </summary>
        /// <param name="mapData"></param>
        /// <param name="mapname"></param>
        /// <param name="tilesize"></param>
        /// <param name="mapsize"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        private static int[] ConvertStringMapToArray(XMLData.MapLayerData mapData, string mapname, int tilesize, Point mapsize, int id)
        {
            String map = mapData.MapTileArray;


            int[] mapTiles = new int[mapsize.Y * mapsize.X];
            map = map.Replace("\t", String.Empty).Replace("\n", String.Empty);
            String[] lines = map.Split(';');

            //delete last one because its empty 
            lines = lines.Where(val => val != String.Empty).ToArray();
            int x = 0;
            int y = 0;
            foreach (var line in lines)
            {

                string[] numbers = line.Split(',');
                foreach (string e in numbers)
                {

                    int tile = 0;


                    if (!int.TryParse(e, out tile))
                    {
                        Console.WriteLine("!----- Error to create tile ID from MapLayer with ID:" + id + ". Name of the map is " + mapname);
                        mapTiles[y * mapsize.X + x] = -1;
                    }
                    else
                    {
                        mapTiles[y * mapsize.X + x] = tile;
                    }

                    

                    




                    x++;
                }
                x = 0;
                y++;
            }


            return mapTiles;
        }

        #endregion

        #region Props

        private int[] layerTiles;
        private int tilesize;
        private byte layerID;
        private bool beforePlayer;

        #endregion

        #region GetterSetter

        internal int[] LayerTiles { get => layerTiles; }
        public byte LayerID { get => layerID; }
        public bool BeforePlayer { get => beforePlayer; }

        #endregion

        #region Methods

       
        /// <summary>
        /// Change a maptile with id q at pos x,y to another maptile id e
        /// </summary>
        /// <param name="ArrayxPos">map pos x</param>
        /// <param name="ArrayyPos">map pos y</param>
        /// <param name="id">new ID for the tile</param>
        /// <param name="loeschen">delete the tile and set tile to null</param>
        /// <param name="mapsize">looks if the tile is in the map</param>
        public void SetIDForTile(int ArrayxPos, int ArrayyPos, ushort id, bool loeschen, Point mapsize)
        {
            if (ArrayyPos < mapsize.Y && ArrayxPos < mapsize.X && ArrayxPos >= 0 && ArrayyPos >= 0)
            {
                if (loeschen)
                {
                    layerTiles[ArrayyPos * mapsize.X + ArrayxPos] = 0;
                    return;
                }


             
                    layerTiles[ArrayyPos * mapsize.X + ArrayxPos] = id;

                Console.WriteLine("Tile  with id " + id + " set to Layer " + this.layerID);
            }
            else
            {
                Console.WriteLine("Tile konnte nicht gesetzt werden da Map zu klein ist bitte Map größe ändern");
            }
        }

        #endregion

    }
}
