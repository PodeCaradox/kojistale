﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace GameEngine.AnimationContent
{
    public class SpriteAnimation
    {

        #region Konstruktor

        public SpriteAnimation(Point startPointinSheet, Point spriteSize, Point tileSize, int animationDelay, int idleDelaymin, int idleDelaymax)
        {
            this.idleDelaymin = idleDelaymin;
            this.idleDelaymax = idleDelaymax;
            this.animationDelay = animationDelay;
            this.spriteSize = spriteSize;
            this.tileSize = tileSize;
            this.startPointinSheet = startPointinSheet;
            actualTile = new Rectangle(startPointinSheet, tileSize);
            lastAnimation = Animation.DownIdle;
            randomIdle = r.Next(idleDelaymin, idleDelaymax);
        }

        #endregion

        #region Props

        private Point spriteSize;
        private Point tileSize;
        private Rectangle actualTile;
        private Point startPointinSheet;
        private int animationDelay;
        private int idleDelaymin;
        private int idleDelaymax;
        private int randomIdle;
        private Random r = new Random();
        private int totalTime;
        private Animation lastAnimation;
        private bool firstTimeIidel = true;
        private static readonly short[] animationLength = { 2, 2, 0, 2, 9, 9, 9, 9 };

        public enum Animation
        {
            DownIdle = 0,
            LeftIdle = 1,
            UpIdle = 2,
            RightIdle = 3,
            Down = 4,
            Left = 5,
            Up = 6,
            Right = 7,
        }

        #endregion

        #region GetterSetter
        public Rectangle ActualTile { get => actualTile; }

        #endregion

        #region Methods

        public void ChangeWalkAnimation(Animation animation, int elapsedTime)
        {

            totalTime += elapsedTime;
            if (totalTime < animationDelay) return;
            firstTimeIidel = true;
            totalTime = 0;
            lastAnimation = (animation - 4);

            if (actualTile.X >= startPointinSheet.X + animationLength[(int)animation] * tileSize.X)
            {
                actualTile.X = startPointinSheet.X;
            }
            else
            {
                actualTile.X = actualTile.X + tileSize.X;
                actualTile.Y = (int)animation * tileSize.Y;
            }

        }

        public void ChangeIdleAnimation(int elapsedTime)
        {
            totalTime += elapsedTime;
            if (totalTime > randomIdle || (totalTime > animationDelay + 40 && startPointinSheet.X != actualTile.X) || firstTimeIidel)
            {
                firstTimeIidel = false;
                totalTime = 0;
                randomIdle = r.Next(idleDelaymin, idleDelaymax);
                if (actualTile.X >= startPointinSheet.X + animationLength[((int)lastAnimation)] * tileSize.X)
                {
                    actualTile.X = startPointinSheet.X;
                    actualTile.Y = ((int)lastAnimation) * tileSize.Y;
                }
                else
                {
                    actualTile.X = actualTile.X + tileSize.X;
                    actualTile.Y = ((int)lastAnimation) * tileSize.Y;
                }
            }


        }

        #endregion

     

    }
}
