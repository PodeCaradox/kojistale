﻿using GameEngine.MapContent;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameEngine.GameObjects
{
    public abstract class Entity
        {
            protected Vector2 gameObjectPos;

            private float depthValue;


            protected bool isActiv = false;

             public  Entity(Vector2 gameObjectPos, float depthValue, bool activ)
            {
                isActiv = activ;
                DepthValue = depthValue;
                this.gameObjectPos = gameObjectPos;
            }

        public Vector2 GameObjectPos { get => gameObjectPos; }
        public float DepthValue
            {
                get => depthValue;
                set => depthValue = value;
            }
        public bool IsActiv { get => isActiv; }

        public abstract void Draw(SpriteBatch spriteBatch);
        public abstract void Update(int elapsedTime, Map map);


        public virtual void SetAcitv(Vector2 gameObjectPos, bool isActiv)
            {
                this.gameObjectPos = gameObjectPos;
                this.isActiv = isActiv;
            }
        }
}
