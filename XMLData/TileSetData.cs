﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XMLData
{
   public  class TileSetData
    {
        public String TextureName;

        public int TileSize;

        public TilePropertyData[] TileProperty;

    }
}
