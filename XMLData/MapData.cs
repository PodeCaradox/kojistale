﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XMLData
{
    public class MapData
    {
        public String TilesetName;

        public int MapHeight;

        public int MapWidth;

        public MapLayerData[] MapLayerData;

        public String CollisionLayer;
    }
}
