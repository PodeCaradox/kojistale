﻿

using GameEngine;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Koji_s_Tale
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        private GraphicsDeviceManager graphics;
        private SpriteBatch spriteBatch;
        /// <summary>

        
        private double timer = 0;
        private float updateTime = 1f / 60;
        private KnightEngine engine;




        //private State _currentState;
        //private State _nextState;


        public Game1()
        {


            // Somewhere in initialisation





            
            this.IsMouseVisible = true;
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            graphics.GraphicsProfile = GraphicsProfile.HiDef;
           
         
            graphics.PreferMultiSampling = true;
            graphics.IsFullScreen = false;
            graphics.PreferredBackBufferWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
            graphics.PreferredBackBufferHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;

            //graphics.PreferredBackBufferWidth = 1000;
            //graphics.PreferredBackBufferHeight = 600;
            IsFixedTimeStep = false;
            graphics.SynchronizeWithVerticalRetrace = true;
            graphics.ApplyChanges();
           


            this.Window.Position = new Point(0, 0);
            //Wen man Borderless angibt  wird grafikarte nich voll benutzt
            this.Window.IsBorderless = false;
            this.Window.Title = "Koji's Tale";
          
          

           

        }
       

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {

            GraphicsDevice.PresentationParameters.MultiSampleCount = 8;
            
                 // Create a new SpriteBatch, which can be used to draw textures.
                 spriteBatch = new SpriteBatch(GraphicsDevice);
            //
            // TODO: use this.Content to load your game content here

            //_currentState = new GameState(this, Content, GraphicsDevice.Viewport);
            //_currentState.LoadContent();
            //_nextState = null;
            engine = new KnightEngine(this);
            engine.LoadMap("testMap");
            Player player = new Player(new Vector2(GraphicsDevice.Viewport.Width/2, GraphicsDevice.Viewport.Height/2),Content,true);
            engine.SetCameraTarget(player);
            engine.AddEntity(player);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        /// 

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            timer += gameTime.ElapsedGameTime.TotalSeconds;


            while (timer >= updateTime)
            {
                FixedTimeUpdate(gameTime);
                timer -= updateTime;


            }


            // TODO: Add your update logic here

            base.Update(gameTime);
        }

        private void FixedTimeUpdate(GameTime gameTime)
        {

            //_currentState.Update(gameTime);
            engine.Update(gameTime);
            //_currentState.PostUpdate(gameTime);

        }

        //public void ChangeState(State state)
        //{
        //    _currentState = state;
        //}

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            //_currentState.Draw(spriteBatch);

            engine.Draw(gameTime, spriteBatch);

            base.Draw(gameTime);
        }
    }
}
