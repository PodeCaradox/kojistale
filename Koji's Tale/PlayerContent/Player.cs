﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameEngine.AnimationContent;
using GameEngine.GameObjects;
using GameEngine.MapContent;
using GameEngine.SharpMath2;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Koji_s_Tale
{
    class Player : Entity
    {

        #region Konstruktor

        public Player(Vector2 pos, ContentManager content,bool activ) : base(pos, pos.Y, activ)
        {

        
            playerSpriteSheet = content.Load<Texture2D>("PlayerSprite");
            font = content.Load<SpriteFont>("Arial");
            //DepthValue = playerSpriteSheet.Height ;
            spriteAnimation = new SpriteAnimation(new Point(0), new Point(playerSpriteSheet.Width, playerSpriteSheet.Height), size.ToPoint(), 40, 1000, 4000);

            collisionRect = new Rect2(new Vector2(0, 25), new Vector2(size.X-5,size.Y));

        }

        #endregion

        #region Props

        private Texture2D playerSpriteSheet;
        private SpriteFont font;
        private SpriteAnimation spriteAnimation;
        private Vector2 size = new Vector2(32, 45);
        private float playerSpeed = 4f;
        private KeyboardState kbState;
        Rect2 collisionRect;

        #if DEBUG
        private bool showCollision = false;
        #endif


        #endregion

        #region Methods

        public override  void Update(int elapsedTime,Map map)
        {
            KeyboardState kbStatebefore = kbState;
            kbState = Keyboard.GetState();
            MovePlayer(elapsedTime, map.CollisionLayer);


        #if DEBUG
        showCollision = map.DrawCollisionLayer;
        #endif

    }




    private void MovePlayer(int elapsedTime, CollisionLayer collisionLayer)
        {
            KeyboardState kbState = Keyboard.GetState();
            bool playerMoved = false;
            var velocitiy = new Vector2(playerSpeed, playerSpeed);
            if (kbState.IsKeyDown(Keys.A))
            {
                velocitiy.Y = 0;
                if (!collisionLayer.IsCollisionWithWorld(gameObjectPos,ref velocitiy, collisionRect,true,null))
                {
                    gameObjectPos -= velocitiy;
                  
                }

                playerMoved = true;
                spriteAnimation.ChangeWalkAnimation(SpriteAnimation.Animation.Left, elapsedTime);
            }
            else if (kbState.IsKeyDown(Keys.D))
            {
                velocitiy.Y = 0;
                if (!collisionLayer.IsCollisionWithWorld(gameObjectPos, ref velocitiy, collisionRect, false, null))
                {
                    gameObjectPos += velocitiy;
                    
                    
                }

                playerMoved = true;
                spriteAnimation.ChangeWalkAnimation(SpriteAnimation.Animation.Right, elapsedTime);
            }
            else if (kbState.IsKeyDown(Keys.W))
            {
                velocitiy.X = 0;
                if (!collisionLayer.IsCollisionWithWorld(gameObjectPos, ref velocitiy, collisionRect, null, true))
                {
                    gameObjectPos -= velocitiy;
                   
                }

                playerMoved = true;
                spriteAnimation.ChangeWalkAnimation(SpriteAnimation.Animation.Up, elapsedTime);
            }
            else if (kbState.IsKeyDown(Keys.S))
            {
                velocitiy.X = 0;
                if (!collisionLayer.IsCollisionWithWorld(gameObjectPos, ref velocitiy, collisionRect, null, false))
                {
                    gameObjectPos += velocitiy;
                }

                playerMoved = true;
                spriteAnimation.ChangeWalkAnimation(SpriteAnimation.Animation.Down, elapsedTime);
            }
            DepthValue = gameObjectPos.Y;

            if(!playerMoved)
            {
                spriteAnimation.ChangeIdleAnimation(elapsedTime);
            }


        }



        public override void Draw(SpriteBatch spriteBatch)
        {
     

            spriteBatch.Draw(playerSpriteSheet, gameObjectPos, spriteAnimation.ActualTile, Color.White);
            Console.WriteLine(spriteAnimation.ActualTile);
            //#if DEBUG
            spriteBatch.DrawString(font, "Pos:"+ gameObjectPos.X + "/"+ gameObjectPos.Y, new Vector2(gameObjectPos.X+10, gameObjectPos.Y-20), Color.Black);

            //if(showCollision) spriteBatch.Draw(GameEngine.rect, gameObjectPos+ collisionRect.Min, new Rectangle(Point.Zero,(collisionRect.Max- collisionRect.Min).ToPoint()), Color.Purple * 0.5f);
            //#endif

        }

        
 





        #endregion

    }
}
